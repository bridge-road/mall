package com.lzb.bemall.vo;

/**
 * @ProjectName: BeMall
 * @Package: com.lzb.bemall.vo
 * @ClassName: RestStatus
 * @Author: LZB
 * @Description:状态描述
 */
public class RestStatus {
    public static final int OK=1000;
    public static final int NO=1001;
}
