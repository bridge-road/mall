package com.lzb.bemall.dao;

import com.lzb.bemall.entity.Users;
import com.lzb.bemall.generate.GeneralDAO;

public interface UsersMapper extends GeneralDAO<Users> {
}