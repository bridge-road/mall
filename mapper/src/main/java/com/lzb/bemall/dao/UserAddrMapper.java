package com.lzb.bemall.dao;

import com.lzb.bemall.entity.UserAddr;
import com.lzb.bemall.generate.GeneralDAO;

public interface UserAddrMapper extends GeneralDAO<UserAddr> {
}