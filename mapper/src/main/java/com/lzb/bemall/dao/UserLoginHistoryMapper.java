package com.lzb.bemall.dao;

import com.lzb.bemall.entity.UserLoginHistory;
import com.lzb.bemall.generate.GeneralDAO;

public interface UserLoginHistoryMapper extends GeneralDAO<UserLoginHistory> {
}