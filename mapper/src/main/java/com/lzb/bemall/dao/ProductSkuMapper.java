package com.lzb.bemall.dao;

import com.lzb.bemall.entity.ProductSku;
import com.lzb.bemall.generate.GeneralDAO;

public interface ProductSkuMapper extends GeneralDAO<ProductSku> {
}