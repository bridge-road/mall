package com.lzb.bemall.dao;

import com.lzb.bemall.entity.Table1;
import com.lzb.bemall.generate.GeneralDAO;

public interface Table1Mapper extends GeneralDAO<Table1> {
}